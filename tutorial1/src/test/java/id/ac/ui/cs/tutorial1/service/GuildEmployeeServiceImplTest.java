package id.ac.ui.cs.tutorial1.service;

import id.ac.ui.cs.tutorial1.model.GuildClerk;
import id.ac.ui.cs.tutorial1.model.GuildEmployee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GuildEmployeeServiceImplTest {


    private GuildEmployeeServiceImpl guildEmployeeService;

    @BeforeEach
    void setUp() {
        guildEmployeeService = new GuildEmployeeServiceImpl();
    }

    @Test
    void testAddValidInputEmployeeWithTypeClerkShouldReturnClerkInstance() {
        GuildEmployee shouldBeClerkInstance = guildEmployeeService.addEmployee("Hans", "MacBeth", "1998-02-21", "Clerk");
        assertTrue(shouldBeClerkInstance instanceof GuildClerk);
    }

    @Test
    void findById() {
    }

    @Test
    void findAll() {
    }
}